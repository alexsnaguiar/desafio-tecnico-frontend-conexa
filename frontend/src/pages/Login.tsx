import React, { Fragment, useEffect, useState } from "react";
import { Box, Button, FormControl, TextField, Typography } from '@mui/material';
import { useNavigate } from "react-router-dom";
import "./Login.css";
import { Header } from "../shared/components/Header";
import boxFrame from "../imagens/setLogin.png"
import IconButton from '@mui/material/IconButton';
import InputAdornment from '@mui/material/InputAdornment';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import HelpOutlineIcon from '@mui/icons-material/HelpOutline';
import api from "../shared/services/api";


interface State {
  email: string;
  password: string;
  showPassword: boolean;
}

const Login = () => {
  const [loading, setLoading] = useState(false);
  const [checkLogin, setCheckLogin] = useState(false);
  const [error, setError] = useState(false);
  const navigate = useNavigate();

  const [values, setValues] = React.useState<State>({
    email: '',
    password: '',
    showPassword: false,
  });

  useEffect(() => {
    if (!loading && checkLogin) {
      navigate("/home");
    }
  }, [loading, navigate]);

  const login = async (email: string, password:string) => {
    setLoading(true);
    try {
      const response = await api.post('/login', {
        email: email,
        password: password
      })
      localStorage.setItem("tokenDesafioConexa", response.data.token);
      localStorage.setItem("nameDesafioConexa", response.data.name);
      setError(false)
      setLoading(false);
      setCheckLogin(true);
    } catch (err: any) {
      setError(true)
      setLoading(false);
    }
    
  };

  const handleClickShowPassword = () => {
    setValues({
      ...values,
      showPassword: !values.showPassword,
    });
  };

  const handleChange =
    (prop: keyof State) => (event: React.ChangeEvent<HTMLInputElement>) => {
      setValues({ ...values, [prop]: event.target.value });
  };

  return (
    
    <Box
        sx={{
          height: '100vh',
          bgcolor: 'background.default',
          // bgcolor: 'red',
        }}
      >
      <div>
        <Header/>
        <div className="login-page">
          <div>
            <Typography variant="h1">Faça Login</Typography>
            <img className="boxFrame" src={boxFrame} alt="Faça Login"/>
          </div>
          <div >
            <Box
              component="form"
              sx={{
                display: "flex",
                flexDirection: "column"
                
              }}
            >
              <TextField 
                value={values.email}
                onChange={handleChange('email')}
                style={{paddingBottom: 20}} 
                id="standard-basic" 
                label="E-mail" 
                variant="standard" 
                placeholder="Digite seu e-mail" 
                
              />
              <TextField
                value={values.password}
                style={{paddingBottom: 20}}
                label={
                  <Fragment>
                    Senha
                    <HelpOutlineIcon style={{paddingLeft: 10}} fontSize="small" />
                  </Fragment>
                }
                variant="standard"
                type={values.showPassword ? 'text' : 'password'}
                error={error}
                helperText={error? 'Email ou senha invalidos' : ' '}
                onChange={handleChange('password')}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <IconButton
                        aria-label="toggle password visibility"
                        onClick={handleClickShowPassword}
                      >
                        {values.showPassword ? <Visibility /> : <VisibilityOff />}
                      </IconButton>
                    </InputAdornment>
                  )
                }}
              />
              <Button onClick={() => login(values.email, values.password)} variant="contained">Entrar</Button>
            </Box>
          </div>
        </div>
      </div>
    </Box>
    
  );
};

export default Login;
