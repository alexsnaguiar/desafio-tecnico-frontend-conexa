import {useEffect, useState } from "react";
import { Button, Typography } from '@mui/material';

import { Header } from "../shared/components/Header";
import "./Home.css";
import certificates from "../imagens/illustration-certificates.png"
import plant from "../imagens/illustration-plant.png"
import api from "../shared/services/api";
import ModalConsultations from "../shared/components/ModalConsultations";



const Home = () => {
  const [consultations, setConsultations] = useState<any[]>([])
  let textConsultations = consultations.length>1? "consultas agendadas" : "consulta agendada"

  
  
  const name = localStorage.getItem('nameDesafioConexa') as string;

  const getConsultations = async () => {
    const token = localStorage.getItem("tokenDesafioConexa")
    try {
      const config = {
        headers: { Authorization: `Bearer ${token}` }
      };
      const response = await api.get('/consultations?_expand=patient', config)

      await setConsultations(response.data)

    } catch (err: any) {
      setConsultations([])
      console.log("ERROR", err)
    }
    
  };

  useEffect(()=>{
    getConsultations()
  }, [textConsultations])
 

 
  return (
    <div className="divHome">
      <Header name={name}/>
      
        <div className="titleHome">
          <Typography variant="h2">Consultas</Typography>
        </div>

        {consultations.length == 0?(
          <div className="bodyHome">
            <div className="divImgPlant">
              <img src={plant} alt="Plant"/>
            </div>
            
            <div className="divMsgEmpty">
              <Typography variant="subtitle1">Não há nenhuma consulta agendada</Typography>
            </div>
            
            <div className="divImgCertificates">
              <img src={certificates} alt="Certificates"/>
            </div>
            
          </div>
        ):(
          <div className="bodyHomeConsultations">
            
            <div style={{marginBottom:"24px"}}>
              <Typography variant="body2">{consultations.length} {textConsultations} </Typography>
            </div>

            <div>

              {consultations.map((item, index) => {
                var date = new Date(item.date)

                  return (
                    <div className="consultations" >
                      <div key={index}>
                          <Typography variant='body1' >
                              {item.patient.first_name} {item.patient.last_name}
                          </Typography>
                          <Typography variant='caption' >
                              {(date.getDate()<10?'0':'') + date.getDate() + '/' + ((date.getMonth() + 1)<10?'0':'') + (date.getMonth() + 1)  + '/' + date.getFullYear() + ' às ' + date.getHours() + ':' + (date.getMinutes()<10?'0':'') + date.getMinutes()  } 
                          </Typography>
                      </div>
                      <div>
                        <Button variant="contained">Atender</Button>
                      </div>
                    </div>
                  )
              })}
              
            </div>
            
            
          </div>
        )}
        
        <div className="footerHome">
          <Button variant="outlined">Ajuda</Button>
          <ModalConsultations reload={getConsultations}/>
        </div>      
      
    </div>
  );
};

export default Home;
