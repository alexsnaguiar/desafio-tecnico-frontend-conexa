import { Box, Typography, Button } from '@mui/material';
import logoImage from "../../imagens/logo-conexa.png"
import "./GlobalComponents.css";
import { useNavigate } from "react-router-dom";


export const Header = (props: { name?: string}) => {
  const navigate = useNavigate();
  let renderName = false

  if (typeof(props.name) !== 'undefined' && props.name != null) {
    renderName = false
  } else {
    renderName = true
  }

  const logout = () => {
    localStorage.removeItem("tokenDesafioConexa");
    localStorage.removeItem("nameDesafioConexa");
    navigate("/");
  };

  return (
    <Box
      sx={{
        width: '99vw',
        paddingTop: "17px",
        bgcolor: 'background.default',
      }}
    >
      <div className={!renderName?'header':'headerHome'}>
        <img
          className='imgLogoHeader'
          src={logoImage}
          alt="Logo"
        />

        {!renderName? (
          <div className='headerUser'>
            <div>
              <Typography variant='subtitle2'>Olá, Dr. {props.name}</Typography>
            </div>
            <div style={{marginLeft: "16px"}}>
              <Button  onClick={logout} variant="outlined">Sair</Button>
            </div>
            
          </div>
        ) : ''}
      </div>
    </Box>
  );
}


