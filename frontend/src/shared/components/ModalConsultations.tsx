import React, { useEffect, useState } from "react";
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import { Autocomplete, TextField } from '@mui/material';
import api from "../../shared/services/api";
import {DateTimePicker} from '@mui/lab'
import { LocalizationProvider } from '@mui/lab'
import AdapterDateFns from '@mui/lab/AdapterDateFns'


const style = {
  position: 'absolute' as 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 300,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 3,
  borderColor: '#2E50D4',
  borderRadius: '20px',
};

type Paciente = {
    id: number
    label: string
}



export default function ModalConsultations(props: { reload?: Function}) {
    const [open, setOpen] = React.useState(false);
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);
    const [patients, setPatients] = useState<any[]>([])
    const [selectedDate, setSelectedDate] = useState<Date | null>(null)

    const [patientSelect, setPatientSelect] = useState<Paciente | null>(null)

    const patientsOptions = patients.map((patient) => ({
        id: patient.id,
        label: patient.first_name +" "+ patient.last_name
    }))
  
  

  const getPatients = async () => {
    const token = localStorage.getItem("tokenDesafioConexa")
    try {
      const config = {
        headers: { Authorization: `Bearer ${token}` }
      };
      const response = await api.get('/patients', config)

      await setPatients(response.data)

    } catch (err: any) {
        setPatients([])
        console.log("ERROR", err)
    }
    
  };
  
  const sendConsultations = async () => {
    const token = localStorage.getItem("tokenDesafioConexa")
    if (patientSelect!= null){
        try {
            const config = {
                headers: { Authorization: `Bearer ${token}` }
            };
            const bodyParameters = {
                patientId: patientSelect.id, date: selectedDate
            };
          const response = await api.post('/consultations', bodyParameters, config)
    
          props.reload?.()
          handleClose()
    
    
        } catch (err: any) {
          console.log("ERROR", err)
        }
    }
    
    
  };

  useEffect(()=>{
    getPatients()
    setPatientSelect(null)
    setSelectedDate(null)
  }, [open])
  
  return (
    <div>
      <Button onClick={handleOpen} variant="contained">Agendar consulta</Button>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
            <div style={{marginBottom: "16px"}}>
                <Typography  variant="subtitle1">Agendar consulta</Typography>
            </div>
            <Box
                component="form"
                sx={{
                    display: "flex",
                    flexDirection: "column"
                }}
                >
                <div className="divAutoComplete">
                    <Autocomplete
                        disablePortal
                        options={patientsOptions}
                        sx={{ width: 300 }}
                        value={patientSelect}
                        onChange={(event: any, newValue: Paciente | null ) => setPatientSelect(newValue)}
                        noOptionsText={"Nenhum paciente encontrado"}
                        renderInput={(params) => <TextField variant="standard" {...params} label="Paciente" />}
                    />
                </div>
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                    <DateTimePicker
                        label='Selecione a data'
                        renderInput={(params) => <TextField variant="standard"{...params} />}
                        value={selectedDate}
                        onChange={(newValue) => {
                            setSelectedDate(newValue)
                        }}
                    />
                    
                </LocalizationProvider>
          
            </Box>
            <div className="divButtonModal">
                <Button onClick={()=>sendConsultations()} variant="contained">Salvar</Button>
            </div>
            
        </Box>
        
      </Modal>
      
    </div>
  );
  
}
