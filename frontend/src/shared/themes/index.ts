import { createTheme } from '@mui/material/styles';

export const theme = createTheme({
    palette:{
        primary:{
            main: '#2E50D4',
            dark: '#233fab',
            light: '#637ddf',
            contrastText: '#ffffff',

        },
        background:{
            default: '#FFFFFB',
            paper: '#FFFFFB'
        },
        text:{
            primary: '#575453'
        },
    },
    typography:{
        h1:{
            fontSize: 52,
            fontWeight: 700,
            paddingBottom: 40,
            letterSpacing: -2.5,
            color: "#1C307F",
            alignSelf: "center",
            '@media (max-width: 768px)': {
                fontSize: 32,
                letterSpacing: -1.5,
            },
        },
        h2:{
            fontSize: 48,
            fontWeight: 700,
            color: "#1C307F",
            paddingLeft: 29,
            '@media (max-width: 768px)': {
                fontSize: 28,
                paddingLeft: 16,
            },
             
        },
        subtitle1:{
            fontSize: 18,
            fontWeight: 700,
            textAlign: "center",
            color: "#999392",
        
        },
        subtitle2:{
            fontSize: 16,
            fontWeight: 600,
            color: "#575453",
            '@media (max-width: 768px)': {
                fontSize: 0,
            },
            fontStyle: "normal",
        
        },
        body1:{
            fontSize: 16,
            fontWeight: 400,
            color: "#575453",
        },
        body2:{
            fontSize: 16,
            fontWeight: 700,
            color: "#575453",
        },
        caption:{
            fontSize: 12,
            fontWeight: 400,
            color: "#575453",
        },
        button: {
            textTransform: 'none',
            borderRadius: 8
        },
        fontFamily: ["Montserrat",].join(",") 
    },
    
});
