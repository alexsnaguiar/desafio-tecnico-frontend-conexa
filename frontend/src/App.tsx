import { ThemeProvider } from '@mui/material';
import { BrowserRouter } from "react-router-dom";
import { AppRoutes } from './routes';
import { theme } from './shared/themes';


export const App = () => {

  
  return (
    <ThemeProvider theme={theme}>
      
      <BrowserRouter>
        <AppRoutes/>
      </BrowserRouter>
    </ThemeProvider>
    
    
  );
}


