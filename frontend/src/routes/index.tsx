import {Routes, Route, Navigate} from 'react-router-dom';
import Home from '../pages/Home';
import Login from '../pages/Login';



const PrivateRoute = ({ children, redirectTo} :any) => {
    const isAuthenticated = localStorage.getItem("tokenDesafioConexa") !== null;
    return isAuthenticated ? children : <Navigate to={redirectTo} />;
};

export function AppRoutes() {

    
    return(
        <Routes>
            <Route
            path="/home"
            element={
                <PrivateRoute redirectTo="/">
                    <Home/>
                </PrivateRoute>
            }
            />
            <Route path="/" element={<Login />} />
            <Route path='*' element={<Navigate to='/'/>}/>
        </Routes>
    );
}

